class Experiment < ApplicationRecord
  has_many :steps, dependent: :destroy

  def percentage_completed
    num_completed = steps.where(completed: true).count
    num_total = steps.count

    percentage = num_completed.to_f / num_total * 100
    (percentage.nan?) ? "0%" : "#{percentage.to_i}%"
  end
end
