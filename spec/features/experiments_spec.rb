require 'rails_helper'

describe 'experiments' do
  before do
    # create 2 experiments
    2.times do |i|
      i += 1
      Experiment.create!({
        name: "Glucose Experiment #{i}",
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
      })
    end

    # create 5 steps for experiment 1
    5.times do |i|
      i += 1
      Experiment.first.steps.create!({
        name: "Step #{i}",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
        completed: (i <= 3) # 3/5 steps will be marked as completed
      })
    end

    # create 8 steps for experiment 2
    8.times do |i|
      i += 1
      Experiment.last.steps.create!({
        name: "Step #{i}",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
        completed: (i <= 2) # 2/8 steps will be marked as completed
      })
    end
  end

  let!(:experiment1) { Experiment.first }
  let!(:experiment2) { Experiment.last }

  context 'experiments index' do
    it 'lists all experiments and their progress' do
      visit 'experiments'

      within "#experiment_#{experiment1.id}" do
        expect(page).to have_content 'Experiment 1'
        expect(page).to have_content '60%'
      end

      within "#experiment_#{experiment2.id}" do
        expect(page).to have_content 'Experiment 2'
        expect(page).to have_content '25%'
      end
    end
  end

  context 'experiment details' do
    it 'lists each step of an experiment along with the step\'s individual status' do
      visit "experiments/#{experiment1.id}"

      expect(page).to have_content 'Experiment 1'
      expect(page).to have_content 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'

      experiment1.steps.each do |step|
        within "#step_#{step.id}" do
          expect(page).to have_content step.name
          expect(page).to have_content((step.completed?) ? 'Complete' : 'Incomplete')
        end
      end
    end
  end
end
