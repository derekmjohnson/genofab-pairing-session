require 'rails_helper'

describe Experiment do
  it "calculates the percentage of an experiment's completed steps" do
    # create experiment
    experiment = Experiment.create!({
      name: "Glucose Experiment",
      description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
    })

    # create experiment's steps
    5.times do |i|
      i += 1
      experiment.steps.create!({
        name: "Step #{i}",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
        completed: (i <= 3) # 3/5 steps will be completed
      })
    end

    expect(experiment.percentage_completed).to eq("60%")
  end
end
