# NOTE: below was copied from spec/features/experiments_spec.rb

# create 2 experiments
2.times do |i|
  i += 1
  Experiment.create!({
    name: "Glucose Experiment #{i}",
    description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
  })
end

# create 5 steps for experiment 1
5.times do |i|
  i += 1
  Experiment.first.steps.create!({
    name: "Step #{i}",
    description: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
    completed: (i <= 3) # 3/5 steps will be marked as completed
  })
end

# create 8 steps for experiment 2
8.times do |i|
  i += 1
  Experiment.last.steps.create!({
    name: "Step #{i}",
    description: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
    completed: (i <= 2) # 2/8 steps will be marked as completed
  })
end
